# Exercices de découverte du SNDS

Bienvenue sur ce dépôt d'introduction à la manipulation du SNDS !
L'objectif est de vous accompagner, grâce à une manipulation guidée, dans la découverte de quelques tables majeures du SNDS.
Grâce aux notebooks hébergés ici, vous pourrez gagner une première sensation des données disponibles dans le SNDS et de leur structure,
en attendant d'avoir accès aux données réelles pour vos projets.

## Les données synthétiques

Afin de faciliter la découverte du SNDS, des données synthétiques avec les mêmes formats et valeurs des données réelles du SNDS ont été générées. Ces données aléatoires, qui n'ont aucune plausibilité médicale, se trouvent dans le [dossier data](https://gitlab.com/healthdatahub/se-former-au-snds/exercices-snds/-/tree/master/data?ref_type=heads) (5000 lignes par fichiers).

Le but premier des exercices est de permettre la familiarisation avec du code en R ou Python pour manipuler des données SNDS, 
mais aussi d'identifier et de mieux comprendre certaines tables et certaines variables centrales dans le SNDS.

:warning: `Concernant les exercices, il est important de garder en tête qu'ils ont été adaptés à des données fictives,
le code des exercices ne peut être utilisé tel quel et nécessitera quelques modifications pour répondre à la réalité du SNDS`

## Les notebooks
Chaque notebook (disponible en R et Python) aborde de manière élémentaire une thématique différente : suivi d’une cohorte, calcul d’indicateurs hospitaliers, identification de diagnostics hospitaliers, etc.

**Attention :**
Il est important de noter que les exercices des notebooks reposent sur des données fictives et que les codes sont :
-	**incomplets** : certains filtres de nettoyage de moindre importance ne sont pas présents (pour plus d’information, reportez-vous à la rubrique dédiée sur la documentation : [Filtres de qualité](https://documentation-snds.health-data-hub.fr/fiches/scalpel_pipeline.html)).
-	**simplifiés** en termes d’approche méthodologique (dans un but de pédagogie)
-	**adaptés** à la structure des données fictives qui ne reflète pas nécessairement celle du vrai SNDS (pour plus d’information, reportez-vous à la rubrique dédiée sur la documentation : [SNDS synthétique](dhttps://documentation-snds.health-data-hub.fr/formation_snds/donnees_synthetiques/)).
-	**non optimisés** pour gagner en temps de requête ; cela peut s’avérer nécessaire lorsqu’on travaille sur le SNDS.

Par conséquent, **le code des exercices ne peut être utilisé tel quel et nécessitera des modifications pour répondre à la réalité du SNDS.**


Les notebooks pré-exécutés sont entreposés sous format *.ipynb* dans ce repository au niveau du dossier notebooks. Vous pouvez directement les consulter sur Gitlab mais vous ne pourrez ni exécuter, ni modifier le code. Pour une utilisation optimale des notebooks, il vous faut télécharger tout le dossier gitlab (notebooks et données) pour ouvrir les notebooks (fichiers *.ipynb*) dans un environnement approprié. Si vous n’êtes pas familier avec ce type de fichier, vous pouvez les importer dans une des applications gratuites suivantes disponibles sur internet : [Google Colaboratory]( https://colab.research.google.com/notebooks/intro.ipynb) ou [JupyterLab](https://jupyter.org/try).


Les notebooks Python ont été développés et testés avec Python 3.7, et les modules suivants :
* *numpy* (1.17)
* *pandas* (0.25)
* *seaborn* (0.9)

Les notebooks R ont été développés et testés avec R 3.6.2, et les modules suivants :
* *reshape2* (1.4.3)
* *dyplr* (0.8.0.1)
* *ggplot2* (3.1.1)
* *data.table* (1.12.2)
* *magrittr* (1.5)
